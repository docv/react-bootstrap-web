import React from "react";
import styled from "styled-components";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import logoIcon from "assets/ocb-omni.png";
import Button from "components/Button";
import appStorage from "utils/appStorage";
import { push } from "store";
import { initAuth, isAuthenticated } from "utils/api";
// import notifier from "utils/notifier";
const Container = styled.div`
  .card {
    border-color: #02733c;
  }
  .card-header {
    background: #02733c;
    text-align: center;
    color: #fff;
  }
  .btn-primary {
    background: #02733c;
    border-color: #02733c;
  }
`;

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.usernameInput = React.createRef();
    this.passwordInput = React.createRef();
    this.state = {
      username: "",
      password: "",
      authenticated: isAuthenticated()
    };
  }
  isValid() {
    if (!this.usernameInput.current) {
      return false;
    }
    if (!this.passwordInput.current) {
      return false;
    }
    let username = this.usernameInput.current.value.trim();
    let password = this.passwordInput.current.value.trim();
    if (!username) {
      return false;
    }
    if (!password) {
      return false;
    }
    return true;
  }
  renderRedirect = () => {
    if (this.state.authenticated) {
      return <Redirect to="/" />;
    }
  };
  auth = payload => {
    let success = resp => {
      console.log("resp", resp);
      if (resp && resp.data && resp.data.accessToken) {
        initAuth(resp.data.accessToken);
      }
      appStorage.set("auth", resp);
      this.setState({
        authenticated: resp && resp.data && resp.data.accessToken
      });
    };
    push({
      type: "auth",
      payload: payload,
      success: success
    });
  };
  onSubmit = e => {
    e.preventDefault();
    if (!this.isValid()) {
      return;
    }
    let username = this.usernameInput.current.value.trim();
    let password = this.passwordInput.current.value.trim();

    this.auth({ username, password });
  };

  render() {
    return (
      <Container className="login mx-auto">
        {this.renderRedirect()}
        <div className="card">
          <div className="card-header">
            <img className="img-fluid" src={logoIcon} alt="ĐĂNG NHẬP" />
            ĐĂNG NHẬP
          </div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="auth-username">Tên Đăng Nhập</label>
                <input
                  ref={this.usernameInput}
                  type="text"
                  className="form-control"
                  id="auth-username"
                  placeholder="Nhập tên đăng nhập"
                  defaultValue=""
                />
              </div>
              <div className="form-group">
                <label htmlFor="auth-password">Mật Khẩu</label>
                <input
                  ref={this.passwordInput}
                  type="password"
                  className="form-control"
                  id="auth-password"
                  placeholder="Nhập mật khẩu"
                  defaultValue=""
                />
              </div>
              <Button
                // disabled={!this.isValid()}
                type="submit"
                className="btn btn-primary btn-block mt-2"
                loading={this.props.loading}
              >
                Đăng Nhập
              </Button>
            </form>
          </div>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.auth && state.auth.loading === true
  // authenticated: state.auth && state.auth.data && state.auth.data.accessToken
});

export default connect(mapStateToProps)(Login);
