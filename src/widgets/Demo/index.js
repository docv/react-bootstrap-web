import React from "react";
import styled from "styled-components";
import Button from "~/components/Button";
import Icon from "@material-ui/icons/Block";
// import { cssClasses } from "~/utils/cssUtils";

const Container = styled.div``;

export default class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      btnLoading: false,
      btnOutline: false,
      btnVariant: 'primary',
      btnSize: ''
    };
  }
  render() {
    return (
      <Container>
        <div className="mb-2 d-flex align-items-center justify-content-center">
          <Button className="mr-2" icon=<Icon fontSize="small" />>Save</Button>
          <Button className="mr-2" variant="secondary" icon=<Icon fontSize="small" />>Save</Button>
          <Button className="mr-2" variant="success" icon=<Icon fontSize="small" />>Save</Button>
          <Button className="mr-2" variant="danger" icon=<Icon fontSize="small" />>Save</Button>
          <Button className="mr-2" variant="warning" icon=<Icon fontSize="small" />>Save</Button>
          <Button className="mr-2" variant="info" icon=<Icon fontSize="small" />>Save</Button>
          <Button className="mr-2" variant="light" icon=<Icon fontSize="small" />>Save</Button>
          <Button className="mr-2" variant="dark" icon=<Icon fontSize="small" />>Save</Button>
          <Button variant="link" icon=<Icon fontSize="small" />>Save</Button>
        </div>

        <div className="mb-2 d-flex align-items-center justify-content-center">
          <Button loading={true} className="mr-2" icon=<Icon fontSize="small" />>Save</Button>
          <Button loading={true} className="mr-2" variant="secondary" icon=<Icon fontSize="small" />>Save</Button>
          <Button loading={true} className="mr-2" variant="success" icon=<Icon fontSize="small" />>Save</Button>
          <Button loading={true} className="mr-2" variant="danger" icon=<Icon fontSize="small" />>Save</Button>
          <Button loading={true} className="mr-2" variant="warning" icon=<Icon fontSize="small" />>Save</Button>
          <Button loading={true} className="mr-2" variant="info" icon=<Icon fontSize="small" />>Save</Button>
          <Button loading={true} className="mr-2" variant="light" icon=<Icon fontSize="small" />>Save</Button>
          <Button loading={true} className="mr-2" variant="dark" icon=<Icon fontSize="small" />>Save</Button>
          <Button loading={true} variant="link" icon=<Icon fontSize="small" />>Save</Button>
        </div>
      </Container>
    );
  }
}
