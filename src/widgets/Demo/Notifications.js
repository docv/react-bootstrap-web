import React from "react";
import styled from "styled-components";
import MonacoEditor from "react-monaco-editor";
import notifier from "utils/notifier";

const Container = styled.div``;

const DEFAULT_NOTIFICATION = {
  type: "primary",
  title: "Welcome on demo!",
  content: "Hey buddy, here you can see what you can do with it.",
  dismiss: {
    showIcon: true,
    click: false,
    duration: 0
  },
  overlay: {
    click: true,
    opacity: 50
  },
  isLoading: true
};

export default class Notifications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: JSON.stringify(DEFAULT_NOTIFICATION, null, 2),
      notification: DEFAULT_NOTIFICATION
    };
  }
  onChange = (newValue, e) => {
    try {
      let notification = JSON.parse(newValue);
      this.setState({
        notification: notification
      });
    } catch (error) {}
  };
  handleChange = e => {
    let target = e.target;
    let value = {};
    value[target.name] = target.value || "";
    this.setState(value);
  };
  editorDidMount = (editor, monaco) => {
    editor.focus();
  };
  pushNotification = () => {
    notifier.push(this.state.notification);
  };
  render() {
    const code = this.state.code;
    const options = {
      selectOnLineNumbers: true
    };
    return (
      <Container>
        <form>
          <div className="mb-3">
            <MonacoEditor
              width="800"
              height="200"
              language="json"
              theme="vs-dark"
              defaultValue={code}
              // value={code}
              options={options}
              onChange={this.onChange}
              editorDidMount={this.editorDidMount}
            />
          </div>
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.pushNotification}
          >
            Push Notification
          </button>
        </form>
      </Container>
    );
  }
}
