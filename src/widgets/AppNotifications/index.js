import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import OpenIcon from "@material-ui/icons/OpenInNew";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/HighlightOff";
import CloseIcon from "@material-ui/icons/Close";
import Button from "components/Button";
import { getValue } from "utils";
import Modal from "react-bootstrap/Modal";
import LangInput from "components/LangInput";
import { push } from "store";

const Container = styled.div``;

class AppNotifications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notification: null,
      readOnly: false,
      activeTitleLanguage: "vi",
      id: null
    };
  }
  componentDidMount() {
    this.getAppNotifications();
  }
  onClose = () => {
    if (!this.props.isSaving) {
      // console.log(this.state.notification);
      this.setState({ notification: null });
      this.getAppNotifications();
    }
  };
  startEdit = () => {
    this.setState({
      readOnly: false
    });
  };
  getAppNotifications = () => {
    push({ type: "getAppNotifications" });
  };
  createAppNotification = payload => {
    if (!payload.distributedType) {
      payload.distributedType = 3;
    }
    if (payload.allOmnier === null || payload.allOmnier === undefined) {
      payload.allOmnier = true;
    }
    if (!payload.type) {
      payload.type = 1;
    }
    console.log("createAppNotification", payload);
    push({ type: "createAppNotification", payload });
  };
  updateAppNotification = payload => {
    if (!payload.distributedType) {
      payload.distributedType = 3;
    }
    if (payload.allOmnier === null) {
      payload.allOmnier = true;
    }
    console.log("updateAppNotification", payload);
    push({ type: "updateAppNotification", payload });
  };
  deleteAppNotifications = ids => {
    push({
      type: "deleteAppNotifications",
      payload: {
        lstIdNoti: ids
      },
      success: () => this.getAppNotifications()
    });
  };
  renderTopBar = () => {
    return (
      <nav className="navbar navbar-expand-lg navbar-light mb-2">
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ol className="breadcrumb my-0 mr-auto">
            <li className="breadcrumb-item">
              <Link to="/app-links">Thông Báo & Khuyến Mãi</Link>
            </li>
          </ol>
          <form className="form-inline my-2 my-lg-0">
            <button
              className="btn btn-link my-2 my-sm-0 border-0 p-0 m-0"
              type="button"
              onClick={() => {
                this.setState({ notification: {} });
              }}
            >
              <AddCircleIcon className="text-primary" />
            </button>
          </form>
        </div>
      </nav>
    );
  };
  doChange = (field, value) => {
    let notification = this.state.notification || {
      distributedType: 3,
      allOmnier: false
    };
    notification[field] = value;
    this.setState({ notification: notification });
  };
  renderModal = () => {
    let notification = this.state.notification;
    return (
      <Modal
        show={!!notification}
        //dialogClassName="modal-full-screen"
        size="lg"
        backdrop="static"
      >
        <Modal.Header className="d-flex align-items-center justify-content-between">
          <div className="lead">
            {notification.id ? "Cập nhập thông báo" : "Tạo mới thông báo"}
          </div>
          {/* <button
            onClick={this.onClose}
            className="btn btn-link p-0 m-0 border-0"
          >
            <CloseIcon fontSize="small" />
          </button> */}
        </Modal.Header>
        <Modal.Body>
          <form>
            <LangInput
              title="Tiêu Đề"
              readOnly={this.state.readOnly}
              defaultValues={{
                Vi: notification.titleVn,
                En: notification.titleEl,
                Kr: notification.titleKr
              }}
              onChange={values => {
                if (values["Vi"]) {
                  this.doChange("titleVn", values["Vi"]);
                }
                if (values["En"]) {
                  this.doChange("titleEl", values["En"]);
                }
                if (values["Kr"]) {
                  this.doChange("titleKr", values["Kr"]);
                }
              }}
            />
            <LangInput
              title="Mô Tả"
              readOnly={this.state.readOnly}
              textarea={true}
              defaultValues={{
                Vi: notification.descriptionVn,
                En: notification.descriptionEl,
                Kr: notification.descriptionKr
              }}
              onChange={values => {
                if (values["Vi"]) {
                  this.doChange("descriptionVn", values["Vi"]);
                }
                if (values["En"]) {
                  this.doChange("descriptionEl", values["En"]);
                }
                if (values["Kr"]) {
                  this.doChange("descriptionKr", values["Kr"]);
                }
              }}
            />
            <LangInput
              title="Nội Dung"
              readOnly={this.state.readOnly}
              textarea={true}
              defaultValues={{
                Vi: notification.bodyVn,
                En: notification.bodyEl,
                Kr: notification.bodyKr
              }}
              onChange={values => {
                if (values["Vi"]) {
                  this.doChange("bodyVn", values["Vi"]);
                }
                if (values["En"]) {
                  this.doChange("bodyEl", values["En"]);
                }
                if (values["Kr"]) {
                  this.doChange("bodyKr", values["Kr"]);
                }
              }}
            />
            <div className="form-group">
              <label>Hình Ảnh</label>
              <input
                type="text"
                readOnly={this.state.readOnly}
                className="form-control"
                placeholder="Nhập thông tin"
                defaultValue={notification.image}
                onChange={e => {
                  this.doChange("image", e.target.value);
                }}
              />
            </div>
            {!this.state.notification.id ? (
              <section>
                <div className="form-group clickable">
                  <label>Loại thông báo</label>
                  <select
                    className="custom-select"
                    defaultValue={1}
                    onChange={e => {
                      this.doChange("type", e.target.value);
                    }}
                  >
                    <option value={1}>Thông Báo</option>
                    <option value={2}>Khuyến Mãi</option>
                  </select>
                </div>
                <div className="form-group">
                  <label>Cách hiển thị thông báo</label>
                  <select
                    className="custom-select"
                    defaultValue={3}
                    onChange={e => {
                      this.doChange("distributedType", e.target.value);
                    }}
                  >
                    <option value={3}>Tất cả</option>
                    <option value={1}>App</option>
                    <option value={2}>Firebase</option>
                  </select>
                </div>
                <div className="d-flex align-item-center mb-2">
                  <div className="custom-control custom-radio custom-control-inline clickable">
                    <input
                      type="radio"
                      id="notification-allOmnier"
                      className="custom-control-input"
                      name="notification-omnier"
                      defaultChecked={true}
                      value={1}
                      onChange={() => {
                        this.doChange("allOmnier", true);
                      }}
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="notification-allOmnier"
                    >
                      Gửi tới tất cả người dùng OMNI
                    </label>
                  </div>
                  <div className="custom-control custom-radio custom-control-inline clickable">
                    <input
                      type="radio"
                      id="notification-lstUserId"
                      name="notification-omnier"
                      className="custom-control-input"
                      value={2}
                      onChange={e => {
                        this.doChange("allOmnier", false);
                      }}
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="notification-lstUserId"
                    >
                      Chỉ định người dùng
                    </label>
                  </div>
                </div>
                {this.state.notification &&
                this.state.notification.allOmnier === false ? (
                  <div>
                    <div className="form-group">
                      <label>Danh sách các người dùng nhận thông báo</label>
                      <input
                        type="text"
                        readOnly={this.state.readOnly}
                        className="form-control"
                        placeholder="Nhập thông tin"
                        defaultValue={notification.lstUserId}
                        onChange={e => {
                          let values = (e.target.value || "").split(/[,\s;]/);
                          let userIds = values
                            .map(value => {
                              return value.trim();
                            })
                            .filter(userId => userId);
                          this.doChange("lstUserId", userIds);
                        }}
                      />
                      <div className="d-flex align-items-center my-2">
                        {(notification.lstUserId || []).map(userId => (
                          <span
                            key={userId}
                            className="badge badge-pill badge-light mr-1 mb-1"
                          >
                            {userId}
                          </span>
                        ))}
                      </div>
                    </div>
                  </div>
                ) : (
                  ""
                )}
              </section>
            ) : (
              ""
            )}

            <div className="d-flex align-items-center justify-content-end">
              <Button
                variant="danger"
                icon={<CloseIcon fontSize="small" />}
                className="mr-2"
                onClick={this.onClose}
              >
                Đóng
              </Button>
              {this.state.readOnly ? (
                <Button
                  onClick={this.startEdit}
                  icon={<EditIcon fontSize="small" />}
                >
                  Sửa
                </Button>
              ) : (
                <Button
                  onClick={() => {
                    if (this.state.notification) {
                      if (this.state.notification.id) {
                        this.updateAppNotification(this.state.notification);
                      } else {
                        this.createAppNotification(this.state.notification);
                      }
                    }
                  }}
                  icon={<EditIcon fontSize="small" />}
                  loading={this.props.isSaving}
                >
                  {this.props.isSaving ? "Đang lưu" : "Lưu"}
                </Button>
              )}
            </div>
          </form>
        </Modal.Body>
      </Modal>
    );
  };
  render() {
    return (
      <Container className="app-notifications">
        {this.renderTopBar()}
        {this.props.isLoading ? (
          <div className="d-flex justify-content-center align-items-center p-4">
            <span
              className="spinner-border spinner-lg text-primary"
              role="status"
            ></span>
          </div>
        ) : (
          ""
        )}
        <div className="list-group list-group-flush">
          {this.props.appNotifications
            ? this.props.appNotifications.map(notification => (
                <div
                  className="list-group-item"
                  key={notification.id}
                  onClick={() => {
                    this.pushNotification(notification.titleVn);
                  }}
                >
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="lead text-muted d-flex align-items-center">
                      <AddIcon fontSize="small" className="mr-2" />
                      <span>{notification.titleVn}</span>
                    </div>
                    <div className="d-inline-flex align-items-center">
                      <Button
                        variant="link"
                        icon={<DeleteIcon fontSize="small" />}
                        className="p-0 m-0 border-0 mr-2 text-danger"
                        loading={this.state.id === notification.id}
                        onClick={() => {
                          this.setState({
                            id: notification.id
                          });
                          this.deleteAppNotifications([notification.id]);
                        }}
                      ></Button>
                      <Button
                        variant="link"
                        link="/version/edit"
                        icon={<OpenIcon fontSize="small" />}
                        className="p-0 m-0 border-0"
                        onClick={() => {
                          this.setState({ notification: notification });
                        }}
                      ></Button>
                    </div>
                  </div>
                  <p>{notification.descriptionVn}</p>
                </div>
              ))
            : ""}
        </div>
        {this.state.notification ? this.renderModal() : ""}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  appNotifications: getValue(state, "getAppNotifications", "data") || [],
  isLoading: state.getAppNotifications && state.getAppNotifications.loading,
  isSaving:
    (state.createAppNotification && state.createAppNotification.loading) ||
    (state.updateAppNotification && state.updateAppNotification.loading) ||
    (state.deleteAppNotifications && state.deleteAppNotifications.loading)
});

export default connect(mapStateToProps)(AppNotifications);
