import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import SaveIcon from "@material-ui/icons/Done";
import BacklIcon from "@material-ui/icons/ArrowBack";
import Button from "~/components/Button";
import { getValue } from "~/utils";
import { push } from "store";

const Container = styled.div``;

class Edit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: "view"
    };
    this.androidVersionNameInput = React.createRef();
    this.androidVersionCodeInput = React.createRef();
    this.androidVersionDateInput = React.createRef();
    this.androidVersionTypeInput = React.createRef();
    this.iosVersionNameInput = React.createRef();
    this.iosVersionCodeInput = React.createRef();
    this.iosVersionDateInput = React.createRef();
    this.iosVersionTypeInput = React.createRef();
  }
  getAppVersion = () => {
    push({
      type: "getAppVersion"
    });
  };
  updateAppVersion = payload => {
    push({
      type: "updateAppVersion",
      payload
    });
  };
  isValid() {
    let inputs = [
      "androidVersionNameInput",
      "androidVersionCodeInput",
      "androidVersionDateInput",
      "androidVersionTypeInput",
      "iosVersionNameInput",
      "iosVersionCodeInput",
      "iosVersionDateInput",
      "iosVersionTypeInput"
    ];
    for (let i = 0; i < inputs.length; i++) {
      let input = inputs[i];
      if (!this[input].current) {
        return false;
      }
      let value = this[input].current.value.trim();
      if (!value) {
        return false;
      }
    }
    return true;
  }
  onSubmit = e => {
    e.preventDefault();
    if (!this.isValid()) {
      return;
    }
    this.updateAppVersion({
      android: {
        CurrenVersionCode: this.androidVersionCodeInput.current.value,
        CurrentVersionName: this.androidVersionNameInput.current.value,
        date: this.androidVersionDateInput.current.value,
        message: this.androidVersionTypeInput.current.value
      },
      ios: {
        CurrenVersionCode: this.iosVersionCodeInput.current.value,
        CurrentVersionName: this.iosVersionNameInput.current.value,
        date: this.iosVersionDateInput.current.value,
        message: this.iosVersionTypeInput.current.value
      }
    });
  };
  render() {
    return (
      <Container className="app-version">
        <div className="list-group list-group-flush">
          <div className="list-group-item">
            <h5 className="mb-1">Android</h5>
            <table className="table">
              <tbody>
                <tr>
                  <td className="text-muted w-100px">Name</td>
                  <td>
                    <input
                      ref={this.androidVersionNameInput}
                      className="form-control"
                      placeholder="Nhập thông tin"
                      defaultValue={getValue(
                        this.props,
                        "appVersion",
                        "android",
                        "CurrentVersionName"
                      )}
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Code</td>
                  <td>
                    <input
                      ref={this.androidVersionCodeInput}
                      className="form-control"
                      placeholder="Nhập thông tin"
                      defaultValue={getValue(
                        this.props,
                        "appVersion",
                        "android",
                        "CurrenVersionCode"
                      )}
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Date</td>
                  <td>
                    <input
                      ref={this.androidVersionDateInput}
                      className="form-control"
                      placeholder="Nhập thông tin"
                      defaultValue={getValue(
                        this.props,
                        "appVersion",
                        "android",
                        "date"
                      )}
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Type</td>
                  <td>
                    <input
                      ref={this.androidVersionTypeInput}
                      className="form-control"
                      placeholder="Nhập thông tin"
                      defaultValue={getValue(
                        this.props,
                        "appVersion",
                        "android",
                        "message"
                      )}
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div href="#" className="list-group-item">
            <h5 className="mb-1">IOS</h5>
            <table className="table">
              <tbody>
                <tr>
                  <td className="text-muted w-100px">Name</td>
                  <td>
                    <input
                      ref={this.iosVersionNameInput}
                      className="form-control"
                      placeholder="Nhập thông tin"
                      defaultValue={getValue(
                        this.props,
                        "appVersion",
                        "ios",
                        "CurrentVersionName"
                      )}
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Code</td>
                  <td>
                    <input
                      ref={this.iosVersionCodeInput}
                      className="form-control"
                      placeholder="Nhập thông tin"
                      defaultValue={getValue(
                        this.props,
                        "appVersion",
                        "ios",
                        "CurrenVersionCode"
                      )}
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Date</td>
                  <td>
                    <input
                      ref={this.iosVersionDateInput}
                      className="form-control"
                      placeholder="Nhập thông tin"
                      defaultValue={getValue(
                        this.props,
                        "appVersion",
                        "ios",
                        "date"
                      )}
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Type</td>
                  <td>
                    <input
                      ref={this.iosVersionTypeInput}
                      className="form-control"
                      placeholder="Nhập thông tin"
                      defaultValue={getValue(
                        this.props,
                        "appVersion",
                        "ios",
                        "message"
                      )}
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="list-group-item d-flex justify-content-end">
            <Button
              link="/version"
              variant="danger"
              icon={<BacklIcon fontSize="small" />}
              className="mr-2"
            >
              Trở Lại
            </Button>
            <Button
              onClick={this.onSubmit}
              icon={<SaveIcon fontSize="small" />}
              loading={this.props.updateAppVersionLoading}
            >
              Lưu
            </Button>
          </div>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  appVersion: getValue(state, "getAppVersion", "data") || {},
  updateAppVersionLoading:
    getValue(state, "updateAppVersion", "loading") === true
});

export default connect(mapStateToProps)(Edit);
