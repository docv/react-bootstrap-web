import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import EditIcon from "@material-ui/icons/Edit";
import Button from "components/Button";
import { getValue } from "utils";
import { push } from "store";
// import notifier from "utils/notifier";

const Container = styled.div``;

class AppVersion extends React.Component {
  componentDidMount() {
    this.getAppVersion();
  }
  getAppVersion = () => {
    push({
      type: "getAppVersion",
      notification: null
    });
  };

  onChange = event => {
    let value = event.target.value;
    try {
      this.setState({
        notification: JSON.parse(value)
      });
    } catch (error) {}
  };

  render() {
    return (
      <Container className="app-version">
        {/* <div className="form-group">
          <textarea
            className="form-control"
            rows="24"
            onChange={this.onChange}
          ></textarea>
          <div className="my-2">
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => {
                if (this.state && this.state.notification) {
                  notifier.push(this.state.notification);
                }
              }}
            >
              Push Notification
            </button>
          </div>
          {JSON.stringify(this.state ? this.state.notification || {} : {})}
        </div> */}

        {this.props.isLoading ? (
          <div className="d-flex justify-content-center align-items-center p-4">
            <span
              className="spinner-border spinner-lg text-primary"
              role="status"
            ></span>
          </div>
        ) : (
          ""
        )}
        <div className="list-group list-group-flush">
          <div className="list-group-item">
            <h5 className="mb-1">Android</h5>
            <table className="table">
              <tbody>
                <tr>
                  <td className="text-muted w-100px">Name</td>
                  <td>
                    {getValue(
                      this.props,
                      "appVersion",
                      "android",
                      "CurrentVersionName"
                    )}
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Code</td>
                  <td>
                    {getValue(
                      this.props,
                      "appVersion",
                      "android",
                      "CurrenVersionCode"
                    )}
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Date</td>
                  <td>
                    {getValue(this.props, "appVersion", "android", "date")}
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Type</td>
                  <td>
                    {getValue(this.props, "appVersion", "android", "message")}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div href="#" className="list-group-item">
            <h5 className="mb-1">IOS</h5>
            <table className="table">
              <tbody>
                <tr>
                  <td className="text-muted w-100px">Name</td>
                  <td>
                    {getValue(
                      this.props,
                      "appVersion",
                      "ios",
                      "CurrentVersionName"
                    )}
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Code</td>
                  <td>
                    {getValue(
                      this.props,
                      "appVersion",
                      "ios",
                      "CurrenVersionCode"
                    )}
                  </td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Date</td>
                  <td>{getValue(this.props, "appVersion", "ios", "date")}</td>
                </tr>
                <tr>
                  <td className="text-muted w-100px">Type</td>
                  <td>
                    {getValue(this.props, "appVersion", "ios", "message")}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="list-group-item d-flex justify-content-end">
            <Button link="/version/edit" icon={<EditIcon fontSize="small" />}>
              Chỉnh Sửa
            </Button>
          </div>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  appVersion: getValue(state, "getAppVersion", "data") || {},
  isLoading: state.getAppVersion && state.getAppVersion.loading
});

export default connect(mapStateToProps)(AppVersion);
