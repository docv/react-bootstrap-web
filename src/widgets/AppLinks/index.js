import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";
import OpenIcon from "@material-ui/icons/OpenInNew";
import EditIcon from "@material-ui/icons/Edit";
import CloseIcon from "@material-ui/icons/Close";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import Button from "components/Button";
import { getValue } from "utils";
import Modal from "react-bootstrap/Modal";
import LangInput from "components/LangInput";
import { push } from "store";

const Container = styled.div``;

class AppLinks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appLink: null,
      readOnly: false,
      activeTitleLanguage: "vi"
    };
  }
  componentDidMount() {
    this.getAppLinks();
  }
  onClose = () => {
    if (!this.props.isSaving) {
      console.log(this.state.appLink);
      this.setState({ appLink: null });
      this.getAppLinks();
    }
  };
  startEdit = () => {
    this.setState({
      readOnly: false
    });
  };
  doChange = (field, value) => {
    let appLink = this.state.appLink || {};
    appLink[field] = value;
    this.setState({ appLink: appLink });
    // console.log("doChange", this.state);
  };
  getAppLinks = () => {
    push({ type: "getAppLinks" });
  };
  saveAppLink = payload => {
    push({ type: "saveAppLink", payload });
  };
  renderModal = () => {
    let appLink = this.state.appLink;
    return (
      <Modal
        show={!!appLink}
        //dialogClassName="modal-full-screen"
        size="lg"
        backdrop="static"
      >
        <Modal.Header className="d-flex align-items-center justify-content-between">
          <div className="lead">{appLink.id ? "Cập nhập" : "Tạo mới"}</div>
          {/* <button
            onClick={this.onClose}
            className="btn btn-link p-0 m-0 border-0"
          >
            <CloseIcon fontSize="small" />
          </button> */}
        </Modal.Header>
        <Modal.Body>
          <form>
            <LangInput
              title="Tiêu Đề"
              defaultValues={{
                Vi: appLink.titleVn,
                En: appLink.titleEl,
                Kr: appLink.titleKr
              }}
              readOnly={this.state.readOnly}
              onChange={values => {
                if (values["Vi"]) {
                  this.doChange("titleVn", values["Vi"]);
                }
                if (values["En"]) {
                  this.doChange("titleEl", values["En"]);
                }
                if (values["Kr"]) {
                  this.doChange("titleKr", values["Kr"]);
                }
              }}
            />
            <LangInput
              title="Mô Tả"
              textarea={true}
              readOnly={this.state.readOnly}
              defaultValues={{
                Vi: appLink.descriptionVn,
                En: appLink.descriptionEl,
                Kr: appLink.descriptionKr
              }}
              onChange={values => {
                if (values["Vi"]) {
                  this.doChange("descriptionVn", values["Vi"]);
                }
                if (values["En"]) {
                  this.doChange("descriptionEl", values["En"]);
                }
                if (values["Kr"]) {
                  this.doChange("descriptionKr", values["Kr"]);
                }
              }}
            />
            <div className="form-group">
              <label>Hình Ảnh</label>
              <input
                type="text"
                className="form-control"
                placeholder="Nhập thông tin"
                defaultValue={appLink.imageUrl}
                readOnly={this.state.readOnly}
                onChange={e => {
                  this.doChange("imageUrl", e.target.value);
                }}
              />
            </div>
            <div className="form-group">
              <label>Liên Kết</label>
              <input
                type="text"
                className="form-control"
                placeholder="Nhập thông tin"
                defaultValue={appLink.href}
                readOnly={this.state.readOnly}
                onChange={e => {
                  this.doChange("href", e.target.value);
                }}
              />
            </div>
            <div className="form-group">
              <label>Loại</label>
              <input
                type="text"
                className="form-control"
                placeholder="Nhập thông tin"
                // readOnly={true}
                defaultValue={appLink.group}
                onChange={e => {
                  this.doChange("group", e.target.value);
                }}
              />
            </div>
            <div className="d-flex align-items-center justify-content-end">
              <Button
                variant="danger"
                icon={<CloseIcon fontSize="small" />}
                className="mr-2"
                onClick={this.onClose}
              >
                Đóng
              </Button>
              {this.state.readOnly ? (
                <Button
                  onClick={this.startEdit}
                  icon={<EditIcon fontSize="small" />}
                >
                  Sửa
                </Button>
              ) : (
                <Button
                  onClick={() => {
                    this.saveAppLink(this.state.appLink);
                  }}
                  icon={<EditIcon fontSize="small" />}
                  loading={this.props.isSaving}
                >
                  {this.props.isSaving ? "Đang lưu" : "Lưu"}
                </Button>
              )}
            </div>
          </form>
        </Modal.Body>
      </Modal>
    );
  };
  renderTopBar = () => {
    return (
      <nav className="navbar navbar-expand-lg navbar-light mb-2">
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ol className="breadcrumb my-0 mr-auto">
            <li className="breadcrumb-item">
              <Link to="/app-links">Links & Banners</Link>
            </li>
          </ol>
          <form className="form-inline my-2 my-lg-0">
            <button
              className="btn btn-link my-2 my-sm-0 border-0 p-0 m-0"
              type="button"
              onClick={() => {
                this.setState({ appLink: {} });
              }}
            >
              <AddCircleIcon className="text-primary" />
            </button>
          </form>
        </div>
      </nav>
    );
  };
  render() {
    return (
      <Container className="app-version">
        {this.renderTopBar()}
        {this.props.isLoading ? (
          <div className="d-flex justify-content-center align-items-center p-4">
            <span
              className="spinner-border spinner-lg text-primary"
              role="status"
            ></span>
          </div>
        ) : (
          ""
        )}
        <div className="list-group list-group-flush">
          {this.props.appLinks
            ? this.props.appLinks.map(appLink => (
                <div className="list-group-item" key={appLink.id}>
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="lead text-muted d-flex align-items-center">
                      <AddIcon fontSize="small" className="mr-2" />
                      <span>{appLink.titleVn}</span>
                    </div>
                    <Button
                      variant="link"
                      link="/version/edit"
                      icon={<OpenIcon fontSize="small" />}
                      className="p-0 m-0 border-0"
                      onClick={() => {
                        this.setState({ appLink: appLink });
                      }}
                    ></Button>
                  </div>
                  <p>{appLink.descriptionVn}</p>
                </div>
              ))
            : ""}
        </div>
        {this.state.appLink ? this.renderModal() : ""}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  appLinks: getValue(state, "getAppLinks", "data") || [],
  isLoading: state.getAppLinks && state.getAppLinks.loading,
  isSaving: state.saveAppLink && state.saveAppLink.loading
});

export default connect(mapStateToProps)(AppLinks);
