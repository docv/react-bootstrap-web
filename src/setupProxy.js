const proxy = require("http-proxy-middleware");

module.exports = function(app) {
  console.log(app);

  app.use(
    proxy("/service", {
      target: "https://mom.phuongdongbank.vn/admin/",
      changeOrigin: true,
      logLevel: "debug"
    })
  );

  /**
   * /Api/... => http://localhost:1313/Api/...
   */
  app.use(
    proxy("/reddit", {
      target: "https://www.reddit.com/r/",
      changeOrigin: true,
      logLevel: "debug",
      pathRewrite: function(path, req) {
        const newPath = path.replace("/reddit", "/") + ".json";
        console.log(path + " => " + newPath);
        return newPath;
      }
    })
  );
  app.use(
    proxy("/newsapi", {
      target: "https://newsapi.org/v2/",
      changeOrigin: true,
      logLevel: "debug",
      pathRewrite: function(path, req) {
        const newPath = path.replace("/newsapi/", "/");
        console.log(path + " => " + newPath);
        return newPath;
      }
    })
  );
  app.use(
    proxy("/api", {
      target: "https://w3s.vn/",
      changeOrigin: true,
      logLevel: "debug"
    })
  );
};
