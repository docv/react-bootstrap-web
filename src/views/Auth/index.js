import React from "react";
import styled from "styled-components";
import Login from "~/widgets/Login";
import { connect } from "react-redux";

const Container = styled.div`
  padding-top: 4rem;
  .login {
    width: 300px;
  }
`;

class Auth extends React.Component {
  render() {
    return (
      <Container>
        <Login />
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  authenticated: state.auth && state.auth.data && state.auth.data.accessToken
});

export default connect(mapStateToProps)(Auth);
