import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch
  // Redirect
} from "react-router-dom";
import PrivateRoute from "components/route/PrivateRoute";
import Home from "views/Home";
import Auth from "views/Auth";
import { connect } from "react-redux";
import AppNotifications from "components/notifications";

class App extends React.Component {
  render() {
    return (
      <Router>
        <AppNotifications />
        <Switch>
          <Route path="/auth" component={Auth} />
          <PrivateRoute path="/" component={Home} />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  messages: state.messages || []
});

export default connect(mapStateToProps)(App);
