// import React from "react";
// import LanguageIcon from "@material-ui/icons/Language";
// import AddIcon from "@material-ui/icons/Add";

export default [
  {
    id: 1,
    text: "Thông Báo & Khuyến Mãi",
    search: "thong bao khuyen mai",
    href: "/app-notifications",
    isLoading: state => {
      return state.getAppNotifications && state.getAppNotifications.loading;
    }
  },
  {
    id: 2,
    text: "Links & Banners",
    search: "links banners",
    href: "/app-links",
    isLoading: state => {
      return state.getAppLinks && state.getAppLinks.loading;
    }
  },
  // {
  //   id: 3,
  //   text: "Đa Ngôn Ngữ",
  //   href: "/translation"
  // },
  {
    id: 4,
    text: "Phiên Bản Ứng Dụng",
    search: "phien ban ung dung",
    href: "/version",
    isLoading: state => {
      return state.getAppVersion && state.getAppVersion.loading;
    }
  },
  // {
  //   id: 5,
  //   text: "Log Ứng Dụng",
  //   search: "log ung dung",
  //   href: "/logs",
  //   isLoading: state => {
  //     return state.getLogs && state.getLogs.loading;
  //   }
  // }
];
