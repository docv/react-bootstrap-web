import React from "react";
import { Switch } from "react-router-dom";
import Card from "react-bootstrap/Card";
import { FilteredList } from "components/FilteredList";
import AppVersion from "widgets/AppVersion";
import EditAppVersion from "widgets/AppVersion/Edit";
import AppLinks from "widgets/AppLinks";
import AppNotifications from "widgets/AppNotifications";
import Notifications from "widgets/Demo/Notifications";
import logo from "assets/logo.png";
import appLinks from "./appLinks";
import PrivateRoute from "components/route/PrivateRoute";

export default class Home extends React.Component {
  render() {
    return (
      <div className="container-fluid h-100">
        <div className="row h-100">
          <div className="col-12 col-md-3 col-xl-2 bg-light">
            <h4 className="text-center text-muted m-0 p-3">
              <img className="img-fluid" src={logo} alt="OMNI ADMIN" />
            </h4>
            <FilteredList links={appLinks} />
            <br />
            {/* <AvatarIcon size="3rem" /> */}
          </div>

          <div className="col-12 col-md-9 col-xl-10 bg-light">
            {/* <nav className="navbar navbar-expand-lg navbar-light my-2">
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon" />
              </button>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ol className="breadcrumb my-0 mr-auto">
                  <li className="breadcrumb-item">
                    <Link to="/todo?x=1">Netflix</Link>
                  </li>
                  <li className="breadcrumb-item">
                    <Link to="/todo?x=2">Zillow Group</Link>
                  </li>
                  <li className="breadcrumb-item active" aria-current="page">
                    Data
                  </li>
                </ol>
              </div>
            </nav> */}
            <Card className="border-light mt-2">
              <Card.Body>
                <Switch>
                  {
                    <PrivateRoute
                      path="/version/edit"
                      component={EditAppVersion}
                    />
                  }
                  {<PrivateRoute path="/version" component={AppVersion} />}
                  {<PrivateRoute path="/app-links" component={AppLinks} />}
                  {<PrivateRoute path="/demo/notifications" component={Notifications} />}
                  {
                    <PrivateRoute
                      path="/app-notifications"
                      component={AppNotifications}
                    />
                  }
                  {<PrivateRoute path="/" component={AppVersion} />}
                </Switch>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}
