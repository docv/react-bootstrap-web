export default ({ getState }) => {
  return next => cmd => {
    console.log("will dispatch", cmd, getState());

    // Call the next dispatch method in the middleware chain.
    const returnValue = next(cmd);

    console.log("state after dispatch", getState());

    // This will likely be the cmd itself, unless
    // a middleware further in chain changed it.
    return returnValue;
  };
};
