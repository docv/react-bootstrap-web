export function getValue(object, ...keys) {
  // console.log(object, keys);
  let value = object;
  for (let i = 0; i < keys.length; i++) {
    let key = keys[i];
    if (value[key]) {
      value = value[key];
    } else {
      return "";
    }
  }
  return value;
}