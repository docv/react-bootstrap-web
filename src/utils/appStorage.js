const storage = window.sessionStorage;

/**
 * delete default storages for security
 */
delete window.localStorage;
delete window.sessionStorage;

class AppStorage {
  set(key, value) {
    if (!value) {
      storage.removeItem(key);
    } else {
      storage.setItem(key, JSON.stringify(value || {}));
    }
  }

  get(key, defaultValue = null) {
    const value = storage.getItem(key);
    if (value) {
      return JSON.parse(value);
    }
    return defaultValue;
  }

  clear() {
    storage.clear();
  }

  getAccessToken() {
    let auth = this.get("auth", {});
    return auth.data ? auth.data.accessToken : null;
  }
}

const appStorage = new AppStorage();

export default appStorage;
