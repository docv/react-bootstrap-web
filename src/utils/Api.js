import { create } from "apisauce";
import { appConfig } from "configs";

let _accessToken = null;
const timeout = appConfig.timeout;

export const api = create({
  baseURL: appConfig.baseURL,
  // headers: {
  //   Authorization: (() => {
  //     if (_accessToken) {
  //       return "Bearer " + _accessToken;
  //     }
  //     return "";
  //   })()
  // }
});

export default method => (path, params) => {
  console.log("API ", { path, params, _accessToken });
  method = (method || "get").trim().toLowerCase();
  if (!api[method]) {
    throw new Error("Request Method Not Supported: " + method);
  }
  if (_accessToken) {
    api.setHeaders({ Authorization: 'Bearer ' + _accessToken });
  }

  const request = api[method](path, params);
  const timer = new Promise(resolve =>
    setTimeout(() => resolve(false), timeout)
  );
  return Promise.race([request, timer]).then(resp => {
    return resp;
  });
};

export const initAuth = accessToken => {
  // console.log("initAuth", accessToken);
  _accessToken = accessToken;
};

export const isAuthenticated = () => {
  return !!_accessToken;
};
