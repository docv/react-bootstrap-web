// import React from "react";
import { store } from "components/notifications";

const merge = require("lodash/merge");

const DEFAULT_OPTIONS = {
  // title: "Hi There!!!",
  // message: "Notifications can be customized to suit your needs",
  // content: (
  //   <div className="d-flex align-items-center">
  //     <div
  //       style={{
  //         width: "2rem",
  //         height: "2rem"
  //       }}
  //       className="mr-3"
  //     >
  //       <div className="spinner-border text-light d-flex">
  //         <span className="sr-only">Loading...</span>
  //       </div>
  //     </div>
  //     <div>Hey buddy, here you can see what you can do with it.</div>
  //   </div>
  // ),
  type: "info",
  container: "top-right",
  insert: "bottom",
  // userDefinedTypes: null,
  dismissable: {
    click: true,
    touch: true
  },
  // dismissIcon: {
  //   className: [],
  //   content: null
  // },
  animationIn: ["animated", "fadeInRight delay-300ms"],
  animationOut: ["animated", "faster", "fadeOutRight"],
  slidingExit: {
    duration: 600,
    timingFunction: "ease-out",
    delay: 0
  },
  slidingEnter: {
    duration: 200,
    cubicBezier: "linear",
    delay: 0
  },
  // slidingExit: {
  //   duration: null,
  //   cubicBezier: null,
  //   delay: null
  // },
  // touchSlidingBack: {
  //   duration: null,
  //   cubicBezier: null,
  //   delay: null
  // },
  // touchSlidingExit: {
  //   duration: null,
  //   cubicBezier: null,
  //   delay: null
  // },
  dismiss: {
    duration: 5000,
    onScreen: true,
    pauseOnHover: true
  },
  width: null
};

const OPTIONS = {
  success: {
    type: "success"
  },
  danger: {
    type: "danger"
  },
  info: {
    type: "info"
  },
  default: {
    type: "default"
  },
  warning: {
    type: "warning"
  }
};

class Notifier {
  push(notification) {
    notification = notification || {};
    const type = (notification.type || "").toLowerCase() || "info";
    let opts = merge({}, DEFAULT_OPTIONS, OPTIONS[type], notification);
    return store.addNotification(opts);
  }
  remove(id) {
    store.removeNotification(id);
  }
}

export default new Notifier();
