const classNames = require("classnames");

export function cssClasses(...args) {
  const classes = {};
  if (args && args.length) {
    args.forEach(arg => {
      if (arg) {
        if (Array.isArray(arg)) {
          //Object.assign(classes, cssClasses(arg));
        } else if (typeof arg === "string") {
          classes[arg] = true;
        } else {
          Object.assign(classes, arg);
        }
      }
    });
  }
  return classNames(classes);
}
