import React from "react";
import styled from "styled-components";
import avatarIcon from "assets/icons/avatar.svg";

export class AvatarIcon extends React.Component {
  render() {
    const Container = styled.div`
      display: inline-flex;
      align-item: center;
      width: ${this.props.size};
      height: ${this.props.size};
      img {
        width: ${this.props.size};
        height: ${this.props.size};
        border-radius: 50%;
      }
    `;
    return (
      <Container>
        <img alt={this.props.alt || ''} src={this.props.src || avatarIcon} />
      </Container>
    );
  }
}
