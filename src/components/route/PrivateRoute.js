import React from "react";
// import { PropTypes } from "prop-types";
import { Route, Redirect } from "react-router-dom";
import appStorage from "utils/appStorage";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const isAuthenticated = !!appStorage.getAccessToken();
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/auth",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
