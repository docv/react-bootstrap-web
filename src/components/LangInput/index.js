import React from "react";
import styled from "styled-components";
import TextareaAutosize from "react-autosize-textarea";

const Container = styled.div``;

class LangInput extends React.Component {
  constructor(props) {
    super(props);
    let language = props.language || "Vi";
    this.state = {
      notification: null,
      editable: false,
      language: language,
      value: (props.defaultValues || {})[language] || "",
      values: props.defaultValues || {}
    };
    this.input = React.createRef();
  }
  componentDidMount() {
    this.textarea && this.textarea.focus();
  }
  onChangeLanguage = language => {
    this.setState({
      language: language,
      value: this.state.values[language] || ""
    });
  };
  onChange = () => {
    let language = this.state.language;
    let values = { ...this.state.values };
    let value = this.input.current.value;
    values[language] = value;
    this.setState({
      value: value,
      values: values
    });
    this.props.onChange && this.props.onChange(values);
  };
  render() {
    let languages = this.props.languages || ["Vi", "En", "Kr"];
    return (
      <Container className="form-group">
        <div className="d-flex align-items-center justify-content-between">
          <span>{this.props.title}</span>
          <div>
            {languages.map(language => (
              <div
                key={language}
                className="custom-control custom-radio custom-control-inline clickable"
              >
                <input
                  type="radio"
                  className="custom-control-input"
                  checked={this.state.language === language}
                  onChange={() => {
                    this.onChangeLanguage(language);
                  }}
                />
                <label
                  onClick={() => {
                    this.onChangeLanguage(language);
                  }}
                  className="custom-control-label"
                >
                  {language}
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="input-group mb-1">
          <div className="input-group-prepend">
            <span className="input-group-text w-48px">
              {this.state.language}
            </span>
          </div>
          {this.props.textarea ? (
            <TextareaAutosize
              className="form-control"
              placeholder="Nhập thông tin"
              maxRows={1000}
              value={this.state.value}
              readOnly={this.props.readOnly === true}
              onChange={this.onChange}
              ref={this.input}
            ></TextareaAutosize>
          ) : (
            <input
              type="text"
              className="form-control"
              placeholder="Nhập thông tin"
              onChange={this.onChange}
              ref={this.input}
              value={this.state.value}
              readOnly={this.props.readOnly === true}
            />
          )}
        </div>
      </Container>
    );
  }
}

LangInput.defaultProps = {
  defaultValues: {
    Vi: "",
    En: "",
    Kr: ""
  },
  language: "Vi"
};

export default LangInput;
