import React from "react";
import styled from "styled-components";
import { cssClasses } from "~/utils/cssUtils";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

const Container = styled.ul``;

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
  }
  render() {
    let { className } = this.props;
    return (
      <Container
        className={cssClasses(className, {
          "list-group": true,
          "list-group-flush": this.props.flush || true,
          hasText: this.state.text,
          "d-flex": true
        })}
      >
        {this.props.items
          ? this.props.items.map((item, index) => {
              return (
                <li
                  key={index}
                  className="list-group-item d-flex align-items-center justify-content-between pr-0"
                >
                  <div className="text-inline">
                    {item.icon || ""}
                    <Link className="text-inline" to={item.href || ""}>
                      {typeof item === "string" ? item : item.text}
                    </Link>
                  </div>
                  {this.props.isLoading(item) ? (
                    <div className="ubox d-inline-flex">
                      <span className="spinner-border text-primary spinner-sm"></span>
                    </div>
                  ) : (
                    ""
                  )}
                </li>
              );
            })
          : ""}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: item => {
    return typeof item.isLoading === "function" && item.isLoading(state);
  }
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(List);
