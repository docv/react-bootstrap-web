import React from "react";
import { PropTypes } from "prop-types";
import ClearIcon from "@material-ui/icons/Cancel";
import styled from "styled-components";
import { cssClasses } from "~/utils/cssUtils";

const Container = styled.div`
  input {
    border: 0;
    padding: 0;
    margin: 0;
    outline: none;
    box-shadow: none !important;
    outline: none !important;
    position: relative;
    flex: 1 1 auto;
    width: 1%;
    margin-bottom: 0;
    &:focus {
    }
  }
  button {
    display: inline-flex;
    outline: none !important;
  }
  .btn-clear {
    display: none;
    color: #d9d9d9;
    &:hover {
      color: #6c757d;
    }
  }
  &.hasText {
    .form-control {
      border-right: 0;
    }
    .btn-clear {
      display: inline-flex;
    }
  }
  .spinner-border {
    width: 1rem;
    height: 1rem;
    margin-right: 0.5rem;
  }
  &.form-control-lg {
    .spinner-border {
      width: 1.25rem;
      height: 1.25rem;
    }
  }
`;

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();
    this.state = {
      value: "",
      loading: false,
      focused: false
    };
  }
  static defaultProps = {
    placeholder: "Tìm kiếm..."
  };
  onChange = () => {
    const value = this.input.current.value;
    this.setState({
      value: value
    });
    if (typeof this.props.onChange === "function") {
      this.props.onChange(value);
    }
  };
  clearInput = () => {
    this.setState({
      value: ""
    });
    this.props.onChange && this.props.onChange("");
    this.input.current && this.input.current.focus();
  };
  renderIcon() {
    if (this.state.loading) {
      return (
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    } else {
      if (this.props.icon) {
        return (
          <button className="btn btn-link p-0 mr-1 border-0 text-muted">
            {this.props.icon}
          </button>
        );
      }
    }
  }
  render() {
    let { className } = this.props;
    return (
      <Container
        className={cssClasses(className, {
          "form-control": true,
          "form-control-lg": this.props.lg,
          hasText: this.state.value,
          "d-flex": true,
          "align-items-center": true,
          focused: this.state.focused
        })}
      >
        {this.renderIcon()}
        <input
          type="text"
          ref={this.input}
          className="border-0 p-0 m-0"
          placeholder={this.props.placeholder}
          value={this.state.value}
          onChange={this.onChange}
          onFocus={() => this.setState({ focused: true })}
          onBlur={() => this.setState({ focused: false })}
        />
        <button className="btn btn-link p-0 m-0 border-0 btn-clear">
          <ClearIcon fontSize="small" onClick={this.clearInput} />
        </button>
      </Container>
    );
  }
}

Input.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  icon: PropTypes.node
};

export default Input;
