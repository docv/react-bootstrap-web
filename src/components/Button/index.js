import React, { Component } from "react";
import { Link } from "react-router-dom";
import { cssClasses } from "~/utils/cssUtils";

export default class Button extends Component {
  renderContent() {
    if (this.props.children) {
      if (this.props.link) {
        return (
          <Link className="text-white" to={this.props.link}>
            {this.props.children}
          </Link>
        );
      } else {
        return <span>{this.props.children}</span>;
      }
    }
  }
  render() {
    let variant = this.props.variant || "primary";
    if (this.props.outline) {
      variant = "outline-" + variant;
    }
    return (
      <button
        className={cssClasses(
          this.props.className,
          "btn d-inline-flex align-items-center justify-content-center",
          "btn-" + variant,
          this.props.size ? "btn-" + this.props.size : ""
        )}
        onClick={this.props.onClick}
        type={this.props.type || "button"}
        disabled={this.props.disabled}
      >
        {this.props.loading ? (
          <span
            className={cssClasses("spinner-border spinner-sm", {
              "mr-2": this.props.children
            })}
            role="status"
          ></span>
        ) : this.props.icon ? (
          <span
            className={cssClasses("d-inline-flex align-items-center", {
              "mr-2": this.props.children
            })}
          >
            {this.props.icon}
          </span>
        ) : (
          ""
        )}
        {this.renderContent()}
      </button>
    );
  }
}
