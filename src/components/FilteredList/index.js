import SearchIcon from "@material-ui/icons/Search";
import Input from "../Input";
import List from "../List";
import React from "react";
import styled from "styled-components";

const Container = styled.div``;

export class FilteredList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      filter: ""
    };
  }
  filter(event) {
    var items = this.props.initialItems || [];
    items = items.filter(function(item) {
      return item.toLowerCase().search(event.target.value.toLowerCase()) !== -1;
    });
    this.setState({ items: items });
  }
  render() {
    return (
      <Container>
        <Input
          icon={<SearchIcon fontSize="small" />}
          onChange={value => {
            this.setState({
              filter: value || ""
            });
          }}
        />
        <List
          className="mt-2"
          items={
            this.props.links.filter(link =>
              link.search.includes((this.state.filter || "").trim().toLowerCase())
            ) || []
          }
        />
      </Container>
    );
  }
}
