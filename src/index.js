import "./index.scss";
import "animate.css/animate.min.css";

import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import App from "views/App";
import * as serviceWorker from "./serviceWorker";
import { reduxStore } from "./store";
import notifier from "./utils/notifier";

window.reduxStore = reduxStore; // just for testing
window.notifier = notifier; // just for testing

ReactDOM.render(
  <Provider store={reduxStore}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
