import appStorage from "utils/appStorage";
import { initAuth } from "utils/api";

const auth = appStorage.get("auth", {});
console.log("auth", { auth });

if (auth.data && auth.data.accessToken) {
  initAuth(auth.data.accessToken);
}

export default {
  auth: auth
};
