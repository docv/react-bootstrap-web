export default (state, cmd) => {
  if (!cmd.type) {
    return state;
  }

  const newState = { ...state };
  let type = cmd.type;
  const remote = cmd.remote;

  if (remote) {
    newState[type] = {
      loading: true
    };
    return newState;
  }

  if (type.endsWith(".data")) {
    type = type.substr(0, type.length - 5);
    newState[type] = {
      ...cmd.data,
      loading: false
    };
    return newState;
  }
  if (type.endsWith(".error")) {
    type = type.substr(0, type.length - 6);
    newState[type] = {
      ...cmd.data,
      loading: false,
      error: true,
      statusCode: cmd.status
    };
    return newState;
  }
  if (!type.startsWith("@@redux")) {
    let action = cmd.action;
    if (typeof action === "function") {
      let nextState = action.call(cmd, state, cmd.params || {});
      console.log("nextState", nextState);
      return nextState;
    }
  }

  delete cmd.type;
  newState[type] = cmd;

  return newState;
};
