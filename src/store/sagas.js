import { all } from "redux-saga/effects";
import { call, put, takeLatest } from "redux-saga/effects";
import api from "utils/api";
import commands from "commands";

function* sendRequest(command) {
  if (!command.remote || !command.remote.path) {
    return;
  }
  const remote = command.remote || {};
  let path = remote.path;
  const method = remote["method"];
  if (command["queryParams"]) {
    let queryParams = command["queryParams"] || [];
    var queryString = Object.keys(queryParams)
      .map(key => {
        return (
          encodeURIComponent(key) + "=" + encodeURIComponent(queryParams[key])
        );
      })
      .join("&");
    path = path + "?" + queryString;
  }
  const result = yield call(api(method), path, command["payload"] || {});

  console.log("sendRequest", { command, result });

  let headers = result.headers;
  let contentType = result.headers["content-type"];

  console.log("headers", headers["content-type"]);

  if (contentType.startsWith("application/json")) {
    if (result.ok) {
      const data = result.data || {};
      yield put({ type: command.type + ".data", data: data });
      if (typeof command.success === "function") {
        command.success(result.data);
      }
    } else {
      yield put({
        type: command.type + ".error",
        data: result.data,
        status: result.status
      });
      if (typeof command.error === "function") {
        command.error(result.data);
      }
    }
  } else {
    console.error("Invalid Content Type: " + contentType);
    //appStorage.clear();
    //window.location.reload();
  }
}

function* commandWatcher(type) {
  yield takeLatest(type, sendRequest);
}

const sagas = [];
for (const type in commands) {
  sagas.push(commandWatcher(type));
}

export default function* rootSaga() {
  yield all(sagas);
}
