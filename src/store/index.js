import rootReducer from "./reducers";
import initState from "./initState";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";
import { logger } from "redux-logger";
import commands from "../commands";

const sagaMiddleware = createSagaMiddleware();
const reduxStore = createStore(
  rootReducer,
  initState,
  applyMiddleware(sagaMiddleware, logger)
);

sagaMiddleware.run(rootSaga);

const dispatch = reduxStore.dispatch;
const push = command => {
  if (!command || !command.type) {
    throw new Error("Invalid Command: " + JSON.stringify(command));
  }
  let type = command.type;
  if (!commands[type]) {
    throw new Error("Invalid Command Type: " + type);
  }
  dispatch(commands[type](command));
};

// just for testing
window.reduxStore = reduxStore;

export { reduxStore, push };
