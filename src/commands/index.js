import auth from "./auth";
import appNotifications from "./appNotifications";
import appVersion from "./appVersion";
import appLinks from "./appLinks";

const options = {
  ...auth,
  ...appVersion,
  ...appNotifications,
  ...appLinks
};

console.log("options", options);

let commands = {};

for (const type in options) {
  commands[type] = option => {
    let defaultOption = options[type];
    // if (typeof defaultOption === "function") {
    //   defaultOption = defaultOption.call(defaultOption, option);
    // }
    let mergedOption = { ...defaultOption, ...option };
    mergedOption["date"] = new Date();
    if (!mergedOption.type) {
      mergedOption["type"] = type;
    }
    return mergedOption;
  };
}

export default commands;
