const getAppLinks = {
  remote: {
    path: "/service/link/find",
    method: "GET"
  }
};

const saveAppLink = {
  remote: {
    path: "/service/link/update",
    method: "POST"
  }
};

export default { getAppLinks, saveAppLink };
