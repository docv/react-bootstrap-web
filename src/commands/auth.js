const auth = {
  remote: {
    path: "/service/auth/login",
    method: "POST"
  }
};

export default { auth };
