const getAppNotifications = {
  remote: {
    path: "/service/promotion/find",
    method: "GET"
  }
};

const createAppNotification = {
  remote: {
    path: "/service/adminNoti/pushNotification",
    method: "POST"
  }
};

const updateAppNotification = {
  remote: {
    path: "/service/promotion/update",
    method: "POST"
  }
};

/**
{"lstIdNoti":[-37,4190,4191]}
 */
const deleteAppNotifications = {
  remote: {
    path: "/service/adminNoti/deleteNotification",
    method: "POST"
  }
};

export default {
  getAppNotifications,
  createAppNotification,
  updateAppNotification,
  deleteAppNotifications
};
