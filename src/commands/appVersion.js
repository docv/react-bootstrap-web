const getAppVersion = {
  remote: {
    path: "/service/appVersion/get",
    method: "GET"
  }
};

const updateAppVersion = {
  remote: {
    path: "/service/appVersion/update",
    method: "POST"
  }
};

export default { getAppVersion, updateAppVersion };
